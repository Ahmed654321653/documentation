1. Review the documentation on [Git](/git), [Composer](/guides/composer), and [Terminus](/terminus), and install and configure them on your local computer. Pantheon requires Composer 2, at minimum.

   - Mac users can use [Homebrew](https://brew.sh/) to install Git, Composer, and PHP, along with their required dependencies. **Note:** Terminus 3 should be used for PHP >= 8.0. Restart the shell or terminal environment after entering the following command:

    ```bash{promptUser:user}
    brew install git composer php
    ```

   - Windows users can install [Composer](https://getcomposer.org/doc/00-intro.md#installation-windows) and [Git](https://git-scm.com/download/win), and may need to install [XAMPP](https://www.apachefriends.org/index.html) or similar to satisfy some dependencies.

1. <Partial file="export-alias.md" />

1. Create a new folder to use while working on the migration. This folder will contain two subdirectories that you'll create in the next sections, one for the site on the former platform, and one for the Pantheon site.
